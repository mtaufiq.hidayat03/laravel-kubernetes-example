<?php

use App\Http\Controllers\LabelController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/', 'App\Http\Controllers\LoginController@index');

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['usersession', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
], function () {
    //using user session middleware
    Route::get('/dashboard','App\Http\Controllers\DashboardController@index');

    /* ROUTE LABEL */
    Route::get('/label', 'App\Http\Controllers\LabelController@index')
        ->name('label');
    Route::get('/label/upload/{woid}', 'App\Http\Controllers\LabelController@upload')
        ->name('label.upload');

    /* ROUTE EMAIL SETTINGS */
    Route::get('/email-settings', 'App\Http\Controllers\EmailNotificationSettingController@index')
        ->name('email-settings');
    Route::get('/email-settings/add', 'App\Http\Controllers\EmailNotificationSettingController@add')
        ->name('email-settings.add');
    Route::get('/email-settings/edit/{id}', 'App\Http\Controllers\EmailNotificationSettingController@edit')
    ->name('email-settings.edit');

    /* ROUTE APPROVAL SETTINGS */
    Route::get('/approval-settings', 'App\Http\Controllers\ApprovalSettingController@index')
        ->name('approval-settings');
    Route::get('/approval-settings/add', 'App\Http\Controllers\ApprovalSettingController@add')
        ->name('approval-settings.add');
    Route::get('/approval-settings/edit/{id}', 'App\Http\Controllers\ApprovalSettingController@edit')
        ->name('approval-settings.edit');

    /* ROUTE APPROVAL HISTORY */
    Route::get('/approval-history','App\Http\Controllers\ApprovalHistoryController@index')
        ->name('approval-history');
    Route::get('/approval-history/add','App\Http\Controllers\ApprovalHistoryController@add')
        ->name('approval-history.add');
    Route::get('/approval-history/edit/{id}', 'App\Http\Controllers\ApprovalHistoryController@edit')
        ->name('approval-history.edit');
});
