<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* API ROUTE USER MANAGEMENT */
Route::get('/user', 'App\Http\Controllers\Api\UserApiController@getAllUsers');
Route::get('/user/{id}', 'App\Http\Controllers\Api\UserApiController@getUser');
Route::post('/user', 'App\Http\Controllers\Api\UserApiController@createUser');
Route::put('/user/{id}', 'App\Http\Controllers\Api\UserApiController@updateUser');
Route::delete('/user/{id}', 'App\Http\Controllers\Api\UserApiController@deleteUser');

/* API ROUTE LOGIN AND LOGOUT USER */
Route::post('/login','App\Http\Controllers\Api\LoginApiController@doLoginAccount');
Route::get('/logout','App\Http\Controllers\Api\LoginApiController@doLogoutAccount');

/* API ROUTE LABEL */
Route::get('/label/list',
    'App\Http\Controllers\Api\LabelApiController@getAllLabel')
    ->name('label.list');

/* API ROUTE DASHBOARD */
Route::get('/dashboard/list',
    'App\Http\Controllers\Api\DashboardApiController@getAllDataDashboard')
    ->name('dashboard.list');

/* API ROUTE EMAIL-SETTINGS */
Route::get('/email-settings/list',
    'App\Http\Controllers\Api\EmailNotificationSettingApiController@getAllEmailNotification')
    ->name('email-settings.list');
Route::post('/email-settings/save-email',
    'App\Http\Controllers\Api\EmailNotificationSettingApiController@saveEmailSettings')
    ->name('email-settings.save-email');
Route::post('/email-settings/update-email',
    'App\Http\Controllers\Api\EmailNotificationSettingApiController@updateEmailSettings')
    ->name('email-settings.update-email');
Route::post('/email-settings/delete-email',
    'App\Http\Controllers\Api\EmailNotificationSettingApiController@deleteEmailSettings')
    ->name('email-settings.delete-email');

/* API ROUTE APPROVAL-SETTINGS */
Route::get('/approval-settings/list',
    'App\Http\Controllers\Api\ApprovalSettingApiController@getAllApprovalSettings')
    ->name('approval-settings.list');
Route::post('/approval-settings/save-approval',
    'App\Http\Controllers\Api\ApprovalSettingApiController@saveApprovalSettings')
    ->name('approval-settings.save-approval');
Route::post('/approval-settings/update-approval',
    'App\Http\Controllers\Api\ApprovalSettingApiController@updateApprovalSettings')
    ->name('approval-settings.update-approval');
Route::post('/approval-settings/delete-approval',
    'App\Http\Controllers\Api\ApprovalSettingApiController@softDeleteApprovalSettings')
    ->name('approval-settings.delete-approval');

/* API ROUTE APPROVAL-HISTORY */
Route::get('/approval-history/list',
    'App\Http\Controllers\Api\ApprovalHistoryApiController@getAllApprovalHistory')
    ->name('approval-history.list');
Route::post('/approval-history/delete-approval-history',
    'App\Htpp\Controllers\Api\ApprovalHistoryApiController@softDeleteApprovalHistory')
    ->name('approval-history.delete-approval-history');
