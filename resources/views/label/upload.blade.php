@extends('layouts.app')
@section('css-section')
    @include('layouts.css-datatables');
    <link rel="stylesheet" href="{{URL::asset('vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css')}}" />
@stop

@section('content')
    <header class="page-header">
        <h2><a href="{{route('label')}}"><i class="fas fa-tags"></i> @lang('label.label')</a> <i
                class="fas fa-angle-right"></i> @lang('label.upload') </h2>
    </header>
    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <section class="card">
                <header class="card-header">
                    <h2>@lang('label.upload')</h2>
                </header>
                <div class="card-body card-body-modified">
                    <form class="form-horizontal form-bordered" action="" id="uploadLabelForm">
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="user_id">WORK ORDER (W/O)
                                ID </label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="woid" name="woid" placeholder="Input WOID"
                                       readonly="readonly" value="{{$label->wo_name}}">
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="user_id">USER ID </label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control" id="user_id" name="user_id"
                                       placeholder="Input user id as approval" readonly="readonly" value="{{Session::get('user_id')}}">
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="set_upload">SET UPLOAD</label>
                            <div class="col-lg-8">
                                <input id="set_upload" type="file" class="file" data-preview-file-type="text" data-msg-placeholder="Select SET FILE for upload...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="set_drawing_upload">SET DRAWING UPLOAD</label>
                            <div class="col-lg-8">
                                <input id="set_drawing_upload" type="file" class="file" data-preview-file-type="text" data-msg-placeholder="Select SET DRAWING FILE for upload">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="box_upload">BOX UPLOAD</label>
                            <div class="col-lg-8">
                                <input id="box_upload" type="file" class="file" data-preview-file-type="text">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="box_drawing_upload">BOX DRAWING UPLOAD</label>
                            <div class="col-lg-8">
                                <input id="box_drawing_upload" type="file" class="file" data-preview-file-type="text" data-msg-placeholder="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 text-left">
                                <a href="{{ URL::previous() }}" class="btn btn-dark" id="back_button">
                                    <i class="back_button_i fas fa-arrow-left"></i> <span class="back-text">Back</span></a>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-lge" id="save_data"
                                        data-loading-text="Saving data..."><i
                                        class="save_data_i fas fa-save"></i>&nbsp;<span class="save-text">Save</span>
                                </button>
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-warning" id="cancel_button"
                                        data-loading-text="Clearing field..."><i
                                        class="cancel_button_i fas fa-trash-alt"></i>&nbsp;<span class="cancel-text">Reset</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    </header>
@stop

@section('js-section')
    <!-- add custom javascript here -->
    <script src="{{ URL::asset('js/custom.js')}}"></script>
    <script src="{{URL::asset('vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js')}}"></script>
    <script type="text/javascript">
        popupConfirmationModal(".buttonModalLogout");
        logoutModalDismiss(".logout-modal-dismiss");
        logoutModalConfirm(".logout-modal-confirm", "GET", "/api-v1/logout");
        nProgressLoading();
        backButton('#back_button');
        cancelButton('#approvalSettingsForm', '#cancel_button');
        saveButton("#save_data", "POST", "/api-v1/approval-settings/save-approval", "#approvalSettingsForm");
        defaultApprovalSelect('#default_approval', '#woid');
        $("#set_upload").fileinput({
            "dropZoneEnabled" : false,
            allowedFileExtensions: ["jpg", "png", "gif"],
        });
        $("#set_drawing_upload").fileinput({
            "dropZoneEnabled" : false,
            allowedFileExtensions: ["jpg", "png", "gif"],
        });
        $("#box_upload").fileinput({
            "dropZoneEnabled" : false,
        });
        $("#box_drawing_upload").fileinput({
            "dropZoneEnabled" : false,
        });
    </script>
    <!-- end custom javascript here -->
@stop

