@extends('layouts.app')
@section('css-section')
    @include('layouts.css-datatables');
@stop

@section('content')
    <header class="page-header">
        <h2><i class="fas fa-check-circle"></i> Approval Settings</h2>
    </header>
    <div class="row">
        <div class="col-lg-12">
            <section class="card">
                <header class="card-header">
                    <!-- <h5> *) IF DEFAULT APPROVAL COLUMN SHOWS "Y" THEN IT WILL BE SET AS DEFAULT APPROVAL</h5> -->
                    <a href="{{route('approval-settings.add')}}" class="mb-1 mt-1 mr-1 btn btn-success"><i
                            class="fas fa-plus-circle"></i> Add Data</a>
                </header>
                <div class="card-body card-body-modified">
                    <table class="table table-bordered approval-settings-datatables">
                        <thead>
                        <tr>
                            <th>NO</th>
                            <th>USER ID</th>
                            <th>WOID</th>
                            <th>DEFAULT APPROVAL</th>
                            <th>CREATED AT</th>
                            <th>UPDATED AT</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </section>
        </div>
    </div>
    </header>
    <div id="modalDeleteApproval"
         class="zoom-anim-dialog modal-block modal-header-color modal-block-danger mfp-hide">
        <section class="card">
            <header class="card-header">
                <h2 class="card-title">Are you sure?</h2>
            </header>
            <div class="card-body">
                <div class="modal-wrapper">
                    <div class="modal-icon">
                        <i class="fas fa-question-circle"></i>
                    </div>
                    <div class="modal-text">
                        <form action="" id="deleteApprovalSettingsForm">
                            <input type="hidden" class="deleteId" name="deleteId" value="">
                        </form>
                        <p class="mb-0">Are you sure to delete the data?</p>
                        <p class="descriptionDelete">
                        </p>
                    </div>
                </div>
            </div>
            <footer class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary" id="delete-modal-confirm"><i class="fas fa-check"></i> Confirm
                        </button>
                        <button class="btn btn-default" id="delete-modal-dismiss"><i class="fas fa-times"></i> Cancel
                        </button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
@stop

@section('js-section')
    @include("layouts.js-datatables")
    <!-- add custom javascript here -->
    <script src="{{ URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript">
        var calcDataTableHeight = function () {
            return $(window).height() * 55 / 100;
        };
        $(window).bind('resize', function () {
            $('.approval-settings-datatables').dataTable().css('width', '100%');
        });
        popupConfirmationModal(".buttonModalLogout");
        logoutModalDismiss(".logout-modal-dismiss");
        logoutModalConfirm(".logout-modal-confirm", "GET", "/api-v1/logout");
        nProgressLoading();
        $(function () {
            var table = $('.approval-settings-datatables').DataTable({
                processing: true,
                paging: true,
                "lengthMenu": [[5, 10, 25, 50, 100, 500, 1000 - 1], [5, 10, 25, 50, 100, 500, 1000, "All"]],
                "pageLength": 10,
                pagingType: "full_numbers",
                serverSide: true,
                "scrollY": calcDataTableHeight(),
                "scrollX": true,
                "scrollCollapse": false,
                timeout: 10000,
                sAjaxSource: "{{ route('approval-settings.list') }}",
                order: [[2, 'asc']],
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'NO',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {data: 'user_id'},
                    {data: 'woid'},
                    {data: 'default_approval'},
                    {
                        data: 'created_at', render: function (data, type, row) {
                            if (!data || data === "") {
                                return "-";
                            } else {
                                return dateFormat(data, "d mmmm yyyy h:MM:ss");
                            }
                        }
                    },
                    {
                        data: 'updated_at', render: function (data, type, row) {
                            if (!data || data === "") {
                                return "-";
                            } else {
                                return dateFormat(data, "d mmmm yyyy h:MM:ss");
                            }
                        }
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                "oLanguage": {
                    "sProcessing": "Processing ....."
                },
                "fnPreDrawCallback": function () {
                    $('.dataTables_processing').attr('style', 'font-size: 16px; font-weight: bold; background: #000000;');
                },
                "fnInitComplete": function (oSettings, json) {
                },
                "fnDrawCallback": function () {
                    popupDeleteConfirmationModal(".buttonModalDeleteApproval", ".descriptionDelete", "WOID", "User ID Approval");
                    deleteModalDismiss("#delete-modal-dismiss");
                    deleteModalConfirm("#delete-modal-confirm", "POST", "/api-v1/approval-settings/delete-approval", ".approval-settings-datatables", "#deleteApprovalSettingsForm");
                },
            });
            var searchWaitInterval = null;
            $('.dataTables_filter input[type=search]').attr('placeholder', 'Looking for data....')
                .off()
                .on('input', function (e) {
                    var item = $(this);
                    var searchWait = 0;
                    if (!searchWaitInterval) searchWaitInterval = setInterval(function () {
                        if (searchWait >= 3) {
                            clearInterval(searchWaitInterval);
                            searchWaitInterval = null;
                            searchTerm = $(item).val();
                            $("#" + item.attr("aria-controls")).DataTable().search(searchTerm).draw();
                            searchWait = 0;
                        }
                        searchWait++;
                    }, 500);

                });
        });
    </script>
    <!-- end custom javascript here -->
@stop
