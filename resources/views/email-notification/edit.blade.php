@extends('layouts.app')
@section('css-section')
    @include('layouts.css-datatables');
@stop

@section('content')
    <header class="page-header">
        <h2><a href="{{route('email-settings')}}"><i class="fas fa-envelope"></i> Email Notification Settings</a> <i class="fas fa-angle-right"></i> Edit Data </h2>
    </header>
    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <section class="card">
                <header class="card-header">
                    <h2>Edit Data</h2>
                </header>
                <div class="card-body card-body-modified">
                    <form class="form-horizontal form-bordered" action="" id="emailSettingsForm">
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="email">Email Address</label>
                            <div class="col-lg-8">
                                <input type="hidden" id="id" name="id" value="{{$email->id}}">
                                <input type="text" class="form-control" id="email" name="email" value="{{$email->email}}" required="required" placeholder="Input the email address">
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-right pt-2" for="divisi">Division</label>
                            <div class="col-lg-8">
                                <select class="form-control mb-3" id="divisi" name="divisi" required="required">
                                    <option value="">Select the division</option>
                                    <option value="SCM" @if ($email->divisi == 'SCM') selected="selected" @endif>SCM</option>
                                    <option value="Production" @if ($email->divisi == 'Production') selected="selected" @endif>Production</option>
                                    <option value="QA" @if ($email->divisi == 'QA') selected="selected" @endif>QA</option>
                                    <option value="Gandaria City" @if ($email->divisi == 'Gandaria City') selected="selected" @endif>Gandaria City</option>
                                    <option value="Other" @if ($email->divisi == 'Other') selected="selected" @endif>Other</option>
                                </select>
                            </div>
                            <div class="col-lg-1">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 text-left">
                                <a href="{{ URL::previous() }}" class="btn btn-dark" id="back_button">
                                    <i class="back_button_i fas fa-arrow-left"></i> <span class="back-text">Back</span></a>
                            </div>
                            <div class="col-sm-6 text-right">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-lge" id="edit_data"><i
                                        class="edit_data_i fas fa-edit"></i>&nbsp;<span class="edit-text">Edit</span></button>
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-warning" id="cancel_button" data-loading-text="Clearing field..."><i
                                        class="cancel_button_i fas fa-undo-alt"></i>&nbsp;<span class="cancel-text">Reset</span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    </header>
@stop

@section('js-section')
    <!-- add custom javascript here -->
    <script src="{{ URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript">
        popupConfirmationModal(".buttonModalLogout");
        logoutModalDismiss(".logout-modal-dismiss");
        logoutModalConfirm(".logout-modal-confirm", "GET", "/api-v1/logout");
        nProgressLoading();
        backButton('#back_button');
        cancelButton('#emailSettingsForm','#cancel_button');
        editButton("#edit_email","POST","/api-v1/email-settings/update-email","#emailSettingsForm");
    </script>
    <!-- end custom javascript here -->
@stop
