<!-- Basic -->
<meta charset="UTF-8">

<title>LG Electronics Indonesia || Label Preparation System</title>
<meta name="keywords" content="LG Electronics Indonesia || Label Preparation System" />
<meta name="description" content="LG Electronics Indonesia || Label Preparation System">
<meta name="author" content="LG Electronics Indonesia">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,800,900|Shadows+Into+Light" rel="stylesheet" type="text/css">
