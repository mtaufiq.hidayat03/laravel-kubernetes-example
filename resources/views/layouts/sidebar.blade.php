<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

    <div class="sidebar-header">
        <div class="sidebar-title">
            Navigation Sidebar
        </div>
        <div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html"
             data-fire-event="sidebar-left-toggle">
            <i class="fas fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <div class="nano">
        <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">

                <ul class="nav nav-main">
                    <li @if (Request::segment(2) == 'dashboard') class="nav-active" @endif>
                        <a class="nav-link" href="/dashboard">
                            <i class="fas fa-home" aria-hidden="true"></i>
                            <span>Dashboard Data Label</span>
                        </a>
                    </li>
                    <li @if (Request::segment(2) == 'label') class="nav-active" @endif>
                        <a class="nav-link" href="/label">
                            <i class="fas fa-tags @if (Request::segment(2) == 'label') faa-horizontal animated @endif" aria-hidden="true"></i>
                            <span>Data Label</span>
                        </a>
                    </li>
                    <li @if (Request::segment(2) == 'approval-history') class="nav-active" @endif>
                        <a class="nav-link" href="/approval-history">
                            <i class="fas fa-history @if (Request::segment(2) == 'approval-history') faa-horizontal animated @endif" aria-hidden="true"></i>
                            <span>Approval History</span>
                        </a>
                    </li>
                    <li @if (Request::segment(2) == 'email-settings') class="nav-active" @endif>
                        <a class="nav-link" href="/email-settings">
                            <i class="fas fa-envelope @if (Request::segment(2) == 'email-settings') faa-horizontal animated @endif" aria-hidden="true"></i>
                            <span>Email Notification Setting</span>
                        </a>
                    </li>
                    <li @if (Request::segment(2) == 'approval-settings') class="nav-active" @endif>
                        <a class="nav-link" href="/approval-settings">
                            <i class="fas fa-check-circle @if (Request::segment(2) == 'approval-settings') faa-horizontal animated @endif" aria-hidden="true"></i>
                            <span>Approval Settings</span>
                        </a>
                    </li>
                    <li @if (Request::segment(2) == 'system-settings') class="nav-active" @endif>
                        <a class="nav-link" href="/system-settings">
                            <i class="fas fa-cogs @if (Request::segment(2) == 'system-settings') faa-horizontal animated @endif" aria-hidden="true"></i>
                            <span>System Settings</span>
                        </a>
                    </li>
                    <li @if (Request::segment(2) == 'log-system') class="nav-active" @endif>
                        <a class="nav-link" href="/log-system">
                            <i class="fas fa-external-link-alt @if (Request::segment(2) == 'log-system') faa-horizontal animated @endif" aria-hidden="true"></i>
                            <span>Log System</span>
                        </a>
                    </li>
                    <li @if (Request::segment(2) == 'my-profile') class="nav-active" @endif>
                        <a class="nav-link" href="/my-profile">
                            <i class="fas fa-user @if (Request::segment(2) == 'my-profile') faa-horizontal animated @endif" aria-hidden="true"></i>
                            <span>My Profile</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link buttonModalLogout" href="#modalLogout">
                            <i class="fas fa-power-off" aria-hidden="true"></i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>

        <script>
            // Maintain Scroll Position
            if (typeof localStorage !== 'undefined') {
                if (localStorage.getItem('sidebar-left-position') !== null) {
                    var initialPosition = localStorage.getItem('sidebar-left-position'),
                        sidebarLeft = document.querySelector('#sidebar-left .nano-content');

                    sidebarLeft.scrollTop = initialPosition;
                }
            }
        </script>


    </div>

</aside>
