<!-- Specific Page Vendor -->
<script src="{{ URL::asset('vendor/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js')}}"></script>
<script src="{{ URL::asset('vendor/datatables/extras/TableTools/Responsive-2.2.0/js/dataTables.responsive.min.js')}}"></script>
