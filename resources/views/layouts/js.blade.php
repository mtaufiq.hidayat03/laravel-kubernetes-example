<!-- Vendor -->
<script src="{{ URL::asset('vendor/jquery/jquery.js')}}"></script>
<script src="{{ URL::asset('vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
<script src="{{ URL::asset('vendor/popper/umd/popper.min.js')}}"></script>
<script src="{{ URL::asset('vendor/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{ URL::asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{ URL::asset('vendor/common/common.js')}}"></script>
<script src="{{ URL::asset('vendor/nanoscroller/nanoscroller.js')}}"></script>
<script src="{{ URL::asset('vendor/magnific-popup/jquery.magnific-popup.js')}}"></script>
<script src="{{ URL::asset('vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
<script src="{{ URL::asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{ URL::asset('vendor/nprogress/nprogress.js')}}"></script>

<!-- Specific Page Vendor -->

<!-- Theme Base, Components and Settings -->
<script src="{{ URL::asset('js/theme.js')}}"></script>


<!-- Theme Initialization Files -->
<script src="{{ URL::asset('js/theme.init.js')}}"></script>

<!-- Add sweetalert2 -->
<script src="{{ URL::asset('vendor/sweetalert2/sweetalert2.all.min.js')}}"></script>
<script src="{{ URL::asset('vendor/sweetalert2/promise.min.js')}}"></script>
