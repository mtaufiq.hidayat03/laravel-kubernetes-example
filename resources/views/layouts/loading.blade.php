<div class="loading-overlay">
    <div class="bounce-loader">
        <div class="row">
            <div class="col-md-12">
                <img src="{{ URL::asset('img/lg_logo.png')}}" height="60" alt="LG Electronics Indonesia"/>
                <img src="{{ URL::asset('img/loading.gif')}}" height="60" alt="loading"/>
            </div>
        </div>
    </div>
</div>
