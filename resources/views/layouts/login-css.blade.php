<!-- add custom style here -->
<style>
    html, body {
        height: 100%;
        background-image: url({{url('img/background.jpg')}});
        background-size:cover;
        background-repeat:no-repeat;
        /*background-color: #ffffff; */
    }

    .body-sign .card-sign .card-body {
        background: #FFFFFF;
        /* filter: alpha(opacity=95);
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";
        -moz-opacity: 0.95;
        -khtml-opacity: 0.95;
        opacity: 0.95; */
    }

    .bottom, .card-title-sign {
        color: #000;
        /* filter: alpha(opacity=95);
        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";
        -moz-opacity: 0.95;
        -khtml-opacity: 0.95;
        opacity: 0.95; */
        padding-top: 10px;
        padding-bottom: 10px;
        border-radius: 15px 15px 15px 15px;
        background-color: #ffffff;
    }

    label {
        color: #fff;
        font-size: 16px;
    }

    a {
        color: #fff;
        font-weight: 500;
    }

    html .btn-primary {
        background-color: #C3023B;
    }

    .password, .username {
        font-size: 1rem;
    }

</style>

<!-- end custom style here -->
