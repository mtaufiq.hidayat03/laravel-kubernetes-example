<!-- Vendor CSS -->
<link rel="stylesheet" href="{{ URL::asset('vendor/bootstrap/css/bootstrap.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('vendor/animate/animate.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('vendor/font-awesome/css/all.min.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('vendor/font-awesome/css/font-awesome-animation.min.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('vendor/magnific-popup/magnific-popup.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('vendor/sweetalert2/animate.min.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('vendor/select2/css/select2.css')}}"/>

<!-- Theme CSS -->
<link rel="stylesheet" href="{{ URL::asset('css/theme.css')}}" />

<!-- Skin CSS -->
<link rel="stylesheet" href="{{ URL::asset('css/skins/default.css')}}" />

<!-- Head Libs -->
<script src="{{ URL::asset('vendor/modernizr/modernizr.js')}}"></script>
