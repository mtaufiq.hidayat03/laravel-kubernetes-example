<!doctype html>
<html class="fixed sidebar-light sidebar-left-collapsed">
<head>

    @include('layouts.meta')

    @include('layouts.css')

    @yield('css-section')

</head>
<body>

    @include('layouts.loading')

    <section class="body">

        <!-- start : Header -->
        @include('layouts.header')
        <!-- end : Header -->

        <div class="inner-wrapper">

            <!-- start: sidebar -->
            @include('layouts.sidebar')
            <!-- end: sidebar -->

            <section role="main" class="content-body">
                <!-- start: page -->
                @yield('content')
                <!-- end: page -->
            </section>
        </div>

    </section>

@include('layouts.js')

@yield('js-section')

</body>
</html>
