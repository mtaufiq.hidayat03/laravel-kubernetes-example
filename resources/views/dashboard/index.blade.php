@extends('layouts.app')
@section('css-section')
    @include('layouts.css-datatables');
@stop

@section('content')
    <header class="page-header">
        <h2><i class="fas fa-home"></i> Dashboard Data Label</h2>
    </header>
    <div class="row">
        <div class="col-lg-12">
            <section class="card">
                <div class="card-body card-body-modified">
                    <table class="table table-bordered dashboard-datatables">
                        <thead>
                        <tr>
                            <th>NO</th>
                            <th>LINE</th>
                            <th>WO</th>
                            <th>MODEL SUFFIX</th>
                            <th>FLAG</th>
                            <th>PRODUCTION DATE</th>
                            <th>SHIP NAME</th>
                            <th>TOTAL QTY</th>
                            <th>REMAIN QTY</th>
                            <th>COMPLETED QTY</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </section>
        </div>
    </div>
    </header>
@stop

@section('js-section')
    @include("layouts.js-datatables")
    <!-- add custom javascript here -->
    <script src="{{ URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript">
        popupConfirmationModal(".buttonModalLogout");
        logoutModalDismiss(".logout-modal-dismiss");
        logoutModalConfirm(".logout-modal-confirm", "GET", "/api-v1/logout");
        nProgressLoading();
        $(function () {
            var table = $('.dashboard-datatables').DataTable({
                processing: true,
                paging: true,
                "lengthMenu": [[5, 10, 25, 50, 100, 500, 1000 -1], [5, 10, 25, 50, 100, 500, 1000, "All"]],
                "pageLength": 10,
                pagingType: "full_numbers",
                serverSide: true,
                "scrollX": true,
                sAjaxSource: "{{ route('dashboard.list') }}",
                order: [[5, 'desc']],
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'NO',
                        orderable: false,
                        searchable: false,
                        render: function (data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {data: 'fa_pcsgid'},
                    {data: 'wo_name'},
                    {data: 'prodid'},
                    {data: 'new_mdl_flag'},
                    {data: 'prdtn_strt_date'},
                    {data: 'ship_to_customer_name'},
                    {data: 'tot_qty'},
                    {data: 'rmn_qty'},
                    {data: 'complt_qty'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                "oLanguage": {
                    "sProcessing": "Processing ....."
                },
                "fnPreDrawCallback": function () {
                    $('.dataTables_processing').attr('style', 'font-size: 16px; font-weight: bold; background: #000000;');
                }
            });
            var searchWaitInterval = null;
            $('.dataTables_filter input[type=search]').attr('placeholder', 'Looking for data....')
                .off()
                .on('input', function (e) {
                    var item = $(this);
                    var searchWait = 0;
                    if (!searchWaitInterval) searchWaitInterval = setInterval(function () {
                        if (searchWait >= 3) {
                            clearInterval(searchWaitInterval);
                            searchWaitInterval = null;
                            searchTerm = $(item).val();
                            $("#" + item.attr("aria-controls")).DataTable().search(searchTerm).draw();
                            searchWait = 0;
                        }
                        searchWait++;
                    }, 500);

                });
        });
    </script>
    <!-- end custom javascript here -->
@stop
