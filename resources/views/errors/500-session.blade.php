<!doctype html>
<html class="fixed sidebar-light sidebar-left-collapsed">
<head>

    @include('layouts.meta')

    @include('layouts.css')

</head>
<body>
<section class="body">

    <!-- start : Header -->
@include('layouts.header')
<!-- end : Header -->

    <div class="inner-wrapper">
        <!-- start: sidebar -->
    @include('layouts.sidebar')
    <!-- end: sidebar -->

        <!-- start: page -->
        <section role="main" class="content-body">
            <header class="page-header">
                <h2>500 | INTERNAL SERVER ERROR</h2>
            </header>
            <section class="body-error error-inside">
                <div class="center-error">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="main-error mb-3">
                                <h2 class="error-code text-dark text-center font-weight-semibold m-0">500 <i
                                        class="fas fa-tired"></i></h2>
                                <p class="error-explanation text-center">We're sorry, something went wrong.</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <img src="{{ URL::asset('img/error_500.png')}}" height="220" alt="Error 500"/>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end: page -->
        </section>
    </div>

</section>

@include('layouts.js')
<!-- add custom javascript here -->
<script src="{{ URL::asset('js/custom.js')}}"></script>
<script type="text/javascript">
    popupConfirmationModal(".buttonModalLogout");
    logoutModalDismiss(".logout-modal-dismiss");
    logoutModalConfirm(".logout-modal-confirm", "GET", "/api-v1/logout");
    nProgressLoading();
</script>
</body>
</html>

