<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApprovalHistory extends Model {
    protected $table = "LPS_APPROVAL_HISTORY";
    public $timestamps = false;
    public $primaryKey  = 'id';
}
