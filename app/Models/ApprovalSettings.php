<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApprovalSettings extends Model {
    protected $table = "LPS_APPROVAL_SETTINGS";
    public $timestamps = false;
    public $primaryKey  = 'id_approval';
}
