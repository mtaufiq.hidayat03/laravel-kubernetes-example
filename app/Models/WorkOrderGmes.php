<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class WorkOrderGmes extends Model
{
    protected $table = "WORKORDER_GMES";
    public $timestamps = false;
}
