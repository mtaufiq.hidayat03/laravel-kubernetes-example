<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{

    protected $table = 'LPS_USERS';
    public $timestamps = false;
}
