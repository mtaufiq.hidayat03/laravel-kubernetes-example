<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailNotification extends Model {

    protected $table = "LPS_EMAIL_NOTIFICATION";
    public $timestamps = false;

}
