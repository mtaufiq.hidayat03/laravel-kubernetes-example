<?php


namespace App\Http\Middleware;

use Closure;

class checkUserSession
{
    public function handle($request, Closure $nextRequest)
    {
        if (!$request->session()->has("user_id")) {
            return redirect('/');
        }

        return $nextRequest($request);
    }

}
