<?php


namespace App\Http\Controllers;


use App\Models\ApprovalHistory;

class ApprovalHistoryController extends Controller
{
    public function index() {
        return view('approval-history.index');
    }

    public function add() {
        return view('approval-history.add');
    }

    public function edit($id) {
        $approvalHistory = ApprovalHistory::where('id', $id)->first();
        return view('approval-histor.edit',['approvalHistory' => $approvalHistory]);
    }
}
