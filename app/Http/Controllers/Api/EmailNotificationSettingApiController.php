<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use App\Models\EmailNotification;
use Route;
use Validator;
use Input;

class EmailNotificationSettingApiController extends Controller
{
    public function getAllEmailNotification(Request $request)
    {
        if ($request->ajax()) {
            $limit = ($request->get('iDisplayLength') ? $request->get('iDisplayLength') : 10);
            $offset = $request->get('iDisplayStart');
            $columnNo = ($request->get('iSortCol_0') ? $request->get('iSortCol_0') : '0');
            $sortBy = ($request->get('sSortDir_0') ? $request->get('sSortDir_0') : 'asc');
            if ($columnNo > 0) {
                $columnNo = $columnNo - 1;
            }
            $columns = ['EMAIL', 'DIVISI', 'ID'];
            $columnName = Str::lower($columns[($columnNo)]);
            $emails = EmailNotification::select($columns)
                ->orderBy($columnName, $sortBy);
            $count_total = $emails->count();
            if (!empty($request->get('sSearch'))) {
                foreach ($columns as $column) {
                    $emails->orWhere($column, 'LIKE', '%' . $request->get('sSearch') . '%');
                }
                $count_filter = $emails->count();
            } else {
                $count_filter = $count_total;
            }
            $data = $emails->get($columns)->skip($offset)->take($limit);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group">' .
                        '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' .
                        'Action</button>' .
                        '<div class="dropdown-menu">' .
                        '<a href="'.URL::to('/email-settings/edit', $row['id']).'" class="dropdown-item edit-data"><i class="edit_data_i fas fa-edit"></i> Edit Data</a>' .
                        '<div class="dropdown-divider"></div>' .
                        ' <a href="#modalDeleteEmail" class="dropdown-item buttonModalDeleteEmail" data-id="'.$row['id'].'"data-default="'.$row['email'].'"><i
                                        class="cancel_button_i fas fa-trash-alt"></i> Delete Data</a>' .
                        '</div></div>';
                    return $actionBtn;
                })
                ->with([
                    "columnNo" => $columnNo,
                    "columnName" => $columnName,
                    "sortBy" => $sortBy,
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function saveEmailSettings(Request $request)
    {
        if ($request->ajax()) {
            $rules = array(
                'email' => 'required',
                'divisi' => 'required'
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json(['message' => "Please, re-checking all fields in this form"], 500);
            } else {
                if (EmailNotification::where('email', $request->email)->exists()) {
                    return response()->json(['message' => "Email: ".$request->email." has already registered, please choose antoher one"], 500);
                } else {
                    try {
                        $emailNotification = new EmailNotification;
                        $emailNotification->email = $request->email;
                        $emailNotification->divisi = $request->divisi;
                        $emailNotification->save();
                        return response()->json(["message" => "Email: ".$request->email." is successfully added to email notification settings",
                            "payload" => URL::to('/email-settings')], 200);
                    } catch (\Exception $e) {
                        return response()->json(["message" => "Error: " . $e->getMessage()], 400);
                    }
                }
            }
        } else {
            return response()->json(['message' => "Forbidden access"], 403);
        }
    }

    public function updateEmailSettings(Request $request)
    {
        if ($request->ajax()) {
            if (EmailNotification::where('id', $request->id)->exists()) {
                $rules = array(
                    'id' => 'required',
                    'email' => 'required',
                    'divisi' => 'required'
                );
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return response()->json(['message' => "Please, re-checking all fields in this form"], 500);
                } else {
                    if (EmailNotification::where('email', $request->email)->exists()) {
                        return response()->json(['message' => "Email: ".$request->email." has already registered, please choose antoher one"], 500);
                    } else {
                        try {
                            $emailNotification = EmailNotification::find($request->id);
                            $emailNotification->email = $request->email;
                            $emailNotification->divisi = $request->divisi;
                            $emailNotification->save();
                            return response()->json(["message" => "Email: ".$request->email." is edited successfully to email notification settings",
                                "payload" => URL::to('/email-settings')], 200);
                        } catch (\Exception $e) {
                            return response()->json(["message" => "Error: " . $e->getMessage()], 400);
                        }
                    }
                }
            } else {
                return response()->json(["message" => "Data not found"], 404);
            }
        } else {
            return response()->json(['message' => "Forbidden access"], 403);
        }
    }

    public function deleteEmailSettings(Request $request)
    {
        if ($request->ajax()) {
            if (EmailNotification::where('id', $request->deleteId)->exists()) {
                try {
                    $emailNotification = EmailNotification::find($request->deleteId);
                    $emailNotification->delete();
                    return response()->json(["message" => "Email notification setting is deleted succefully"], 200);
                } catch (\Exception $e) {
                    return response()->json(["message" => "Error: " . $e->getMessage()], 400);
                }
            } else {
                return response()->json(["message" => "Data not found"], 404);
            }
        } else {
            return response()->json(['message' => "Forbidden access"], 403);
        }
    }
}
