<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class DashboardApiController extends Controller
{
    public function getAllDataDashboard(Request $request)
    {
        if ($request->ajax()) {
            $limit = ($request->get('iDisplayLength') ? $request->get('iDisplayLength') : 10);
            $offset = $request->get('iDisplayStart');
            $columnNo = ($request->get('iSortCol_0') ? $request->get('iSortCol_0') : '0');
            $sortBy = ($request->get('sSortDir_0') ? $request->get('sSortDir_0') : 'asc');
            if ($columnNo > 0) {
                $columnNo = $columnNo - 1;
            }
            $columns = ['WORKORDER_GMES.FA_PCSGID', 'WORKORDER_GMES.WO_NAME', 'WORKORDER_GMES.PRODID',
                'TB_POM_DILY_PRDTN_PLN_GMES.NEW_MDL_FLAG', 'TB_POM_DILY_PRDTN_PLN_GMES.PRDTN_STRT_DATE',
                'TB_POM_DILY_PRDTN_PLN_GMES.SHIP_TO_CUSTOMER_NAME', 'TB_POM_DILY_PRDTN_PLN_GMES.TOT_QTY',
                'TB_POM_DILY_PRDTN_PLN_GMES.RMN_QTY', 'TB_POM_DILY_PRDTN_PLN_GMES.COMPLT_QTY'];
            $columnName = Str::lower($columns[($columnNo)]);
            $workOrders = DB::table('WORKORDER_GMES')
                ->join('TB_POM_DILY_PRDTN_PLN_GMES', 'WORKORDER_GMES.WOID', '=', 'TB_POM_DILY_PRDTN_PLN_GMES.WOID')
                ->select($columns)
                ->where('TB_POM_DILY_PRDTN_PLN_GMES.PRDTN_STRT_DATE',
                    \DB::raw('(SELECT MAX(PRDTN_STRT_DATE) FROM TB_POM_DILY_PRDTN_PLN_GMES WHERE WOID=WORKORDER_GMES.WOID)'))
                ->orderBy($columnName, $sortBy);
            $count_total = $workOrders->count();
            if (!empty($request->get('sSearch'))) {
                foreach ($columns as $column) {
                    $workOrders->orWhere($column, 'LIKE', '%' . $request->get('sSearch') . '%');
                }
                $count_filter = $workOrders->count();
            } else {
                $count_filter = $count_total;
            }
            $data = $workOrders->get($columns)->skip($offset)->take($limit);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group">' .
                        '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' .
                        'Action</button>' .
                        '<div class="dropdown-menu">' .
                        '<a href="javascript:void(0)" class="dropdown-item view-set-label">View Set Label</a>' .
                        '<div class="dropdown-divider"></div>' .
                        ' <a href="javascript:void(0)" class="dropdown-item view-barcode-label">View Barcode Label</a>' .
                        '</div></div>';
                    return $actionBtn;
                })
                ->with([
                    "columnNo" => $columnNo,
                    "columnName" => $columnName,
                    "sortBy" => $sortBy,
                    "recordsTotal" => $count_total,
                    "recordsFiltered" => $count_filter,
                ])
                ->rawColumns(['action'])
                ->make(true);
        }
    }
}
