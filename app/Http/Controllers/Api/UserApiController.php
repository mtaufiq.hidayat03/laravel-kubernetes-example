<?php

namespace App\Http\Controllers\Api;

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserApiController extends Controller
{
    public function getAllUsers()
    {
        $users = User::get()->toJson(JSON_PRETTY_PRINT);
        return response($users, 200);
    }

    public function getUser($id)
    {
        if (User::where('id', $id)->exists()) {
            $user = User::where('id', $id)->get(['user_id', 'password', 'username', 'email'])->toJson(JSON_PRETTY_PRINT);
            return response($user, 200);
        } else {
            return response()->json(["message" => "User not found"], 404);
        }
    }

    public function createUser(Request $request)
    {
        try {
            $user = new User;
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->employee_no = $request->employee_no;
            $user->user_id = $request->user_id;
            $user->password = sha1($request->password);
            $user->user_level = $request->user_level;
            $user->save();
            return response()->json(["message" => "User created"], 200);
        } catch (\Exception $e) {
            return response()->json(["message" => "Error: " + $e->getMessage()], 400);
        }
    }

    public function updateUser(Request $request, $id)
    {
        if (User::where('id', $id)->exists()) {
            try {
                $user = User::find($id);
                $user->firstname = is_null($request->firstname) ? $user->firstname : $request->firstname;
                $user->lastname = is_null($request->lastname) ? $user->lastname : $request->lastname;
                $user->email = is_null($request->email) ? $user->email : $request->email;
                $user->employee_no = is_null($request->employee_no) ? $user->employee_no : $request->employee_no;
                $user->user_id = is_null($request->user_id) ? $user->user_id : $request->user_id;
                $user->password = is_null($request->password) ? $user->password : sha1($request->password);
                $user->user_level = is_null($request->user_level) ? $user->user_level : $request->user_level;
                $user->save();
                return response()->json(["message" => "User deleted"], 202);
            } catch (\Exception $e) {
                return response()->json(["message" => "Error: " + $e->getMessage()], 400);
            }
        } else {
            return response()->json(["message" => "User not found"], 404);
        }
    }

    public function deleteUser($id)
    {
        if (User::where('id', $id)->exists()) {
            try {
                $user = User::find($id);
                $user->delete();
            } catch (\Exception $e) {
                return response()->json(["message" => "Error: " + $e->getMessage()], 400);
            }
        } else {
            return response()->json(["message" => "User not found"], 404);
        }
    }
}
