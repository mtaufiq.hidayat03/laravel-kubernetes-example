<?php

namespace App\Http\Controllers;

use App\Models\Label;
use Session;
use DateTime;
use Carbon\Carbon;
use Mcamara\LaravelLocalization\LaravelLocalization;

class LabelController extends Controller
{

    public function index()
    {
        $now = new DateTime('now');
        $mytime = Carbon::now()->locale('id')->isoFormat('dddd, D MMMM YYYY');
        return view('label.index',['now' => $mytime]);
    }

    public function upload($woid) {
        $label = Label::where('wo_name', $woid)->first();
        return view('label.upload',['label' => $label]);
    }

}
