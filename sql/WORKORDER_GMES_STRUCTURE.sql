DROP TABLE MFG.WORKORDER_GMES CASCADE CONSTRAINTS;

CREATE TABLE MFG.WORKORDER_GMES
(
  WOID                      NVARCHAR2(50)       NOT NULL,
  PRODID                    NVARCHAR2(50),
  BOMREV                    NVARCHAR2(50),
  PLANQTY                   NUMBER(38),
  PLANSTDTTM                TIMESTAMP(6),
  PLANEDDTTM                TIMESTAMP(6),
  WOSTAT                    NVARCHAR2(50),
  SALESMODEL                NVARCHAR2(50),
  INQTY                     NUMBER(38),
  OUTQTY                    NUMBER(38),
  SCRAPQTY                  NUMBER(38),
  CANCELQTY                 NUMBER(38),
  AREAID                    NVARCHAR2(50),
  PCSGID                    NVARCHAR2(50),
  EQSGID                    NVARCHAR2(50),
  CUSTOMERID                NVARCHAR2(50),
  CUSTPRODID                NVARCHAR2(50),
  WOHOLD                    NVARCHAR2(1),
  WOTYPE                    NVARCHAR2(50),
  PRIORITY                  NUMBER(38),
  LOT_CODE                  NVARCHAR2(10),
  WO_NAME                   NVARCHAR2(50),
  ORG_ID                    NVARCHAR2(20),
  SCHD_GR_ID                NVARCHAR2(20),
  STATUS_TYPE               NUMBER,
  STATUS_TYPE_DESC          NVARCHAR2(100),
  SUPPLIERID                NVARCHAR2(50),
  QUANTITY_COMPLETED        NUMBER,
  QUANTITY_SCRAPPED         NUMBER,
  COMPLETION_SUBINVENTORY   NVARCHAR2(10),
  COMPLETION_LOCATOR_ID     NUMBER,
  TOP_MODEL_NAME            NVARCHAR2(50),
  DEMAND_ID                 NVARCHAR2(20),
  DATE_RELEASED             DATE,
  DATE_COMPLETED            DATE,
  DATE_CLOSED               DATE,
  NET_QUANTITY              NUMBER,
  COMPLETION_SOURCE_TYPE    NVARCHAR2(10),
  PRODUCT_GOODS_TYPE_CODE   NVARCHAR2(5),
  PRODUCT_GROUP_CODE        NVARCHAR2(30),
  PROJECT_CODE              NVARCHAR2(15),
  WIP_SUPPLY_TYPE           NUMBER,
  OSP_FLAG                  NVARCHAR2(1),
  ALTERNATE_BOM_DESIGNATOR  NVARCHAR2(10),
  FACTORY_CODE              NVARCHAR2(10),
  FACTORY_NAME              NVARCHAR2(240),
  WO_CLASS_CODE             NVARCHAR2(10),
  DEPARTMENT_ID             NUMBER,
  DEPARTMENT_CLASS_CODE     NVARCHAR2(10),
  WO_STD_TIME               NUMBER,
  FA_PCSGID                 NVARCHAR2(10),
  FA_STRT_DATE              DATE,
  REWORK_ORIGINAL_ASSEMBLY  NVARCHAR2(50),
  REWORK_ORIGINAL_WO        NVARCHAR2(240),
  NOTES                     NVARCHAR2(2000),
  MGR_NOTES                 NVARCHAR2(2000),
  ITEM_DESC                 NVARCHAR2(300),
  ITM_TYPE                  NVARCHAR2(30),
  PRODTYPE                  NVARCHAR2(10),
  OQC_FLAG                  NVARCHAR2(1),
  CURR_PRDTN_FLAG           NVARCHAR2(1),
  SOURCE_CODE               NVARCHAR2(30),
  RSLT_PRCS_MTHD_CODE       NVARCHAR2(10),
  MAN_RSLT_QTY              NUMBER,
  MKT_TP_CODE               NVARCHAR2(10),
  CHG_PCSGID                NVARCHAR2(50),
  PLN_QTY_OVER_ALLW_FLAG    NVARCHAR2(1),
  DIR_PRT_FLAG              NVARCHAR2(1),
  PRDTN_STRT_DATE           DATE,
  PRDTN_LAST_DATE           DATE,
  BOX_DPLY_EXCL_FLAG        NVARCHAR2(1),
  ERP_LAST_UPDATE_DATE      DATE,
  ERP_CREATION_DATE         DATE,
  INSUSER                   NVARCHAR2(50),
  INSDTTM                   TIMESTAMP(6),
  UPDUSER                   NVARCHAR2(50),
  UPDDTTM                   TIMESTAMP(6)
)
TABLESPACE MFG_DAT
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE;


CREATE INDEX MFG.IDX_WORKORDER_GMES_01 ON MFG.WORKORDER_GMES
(WOID)
LOGGING
TABLESPACE MFG_DAT
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           );
